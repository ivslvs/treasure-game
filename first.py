def get_treasure_map(data):
    matrix = [([0] * 6) for i in range(6)]

    for row in range(1, len(matrix)):
        for col in range(1, len(matrix)):
            matrix[row][col] = data[row - 1][col - 1]
    return matrix


def get_treasure(t_map):
    cell_list = []

    def treasure(row=1, col=1):  # closure
        value = t_map[row][col]
        concat_index = int(str(row) + str(col))
        cell_list.append(concat_index)

        if len(cell_list) == 5 * 5:  # treasure map size
            return "NO TREASURE"
        elif value == concat_index:
            return '\n'.join(map(str, cell_list))
        else:
            return treasure(int(str(value)[0]), int(str(value)[1]))  # recursion
    return treasure


def get_game_result(data):
    treasure_map = get_treasure_map(data)
    game_result = get_treasure(treasure_map)()
    return game_result


if __name__ == "__main__":
    import sys

    input_line_counter, input_data = 0, []
    for line in sys.stdin:
        input_data.append([int(i) for i in line.split()])
        if input_line_counter == 5:
            break
        input_line_counter += 1

    result = get_game_result(input_data)
    print(result)
