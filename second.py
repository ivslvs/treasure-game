class TreasureMap:
    """Build Treasure Map"""

    def __init__(self, data):
        self.input_data = data
        self.__matrix = [([0] * 6) for _ in range(6)]  # name mangling to avoid attribute rewrite

    def get_treasure_map(self):
        for row in range(1, len(self.__matrix)):
            for column in range(1, len(self.__matrix)):
                self.__matrix[row][column] = self.input_data[row - 1][column - 1]
        return self.__matrix


class Treasure:
    """Treasure object"""

    def __init__(self):
        self.row = 1
        self.column = 1
        self.value = 0
        self.concatenate_index = ''

    def find_treasure(self, treasure_map):
        cell_list = []

        while True:
            self.value = treasure_map[self.row][self.column]
            self.concatenate_index = int(str(self.row) + str(self.column))
            cell_list.append(int(self.concatenate_index))

            self.row = int(str(self.value)[0])
            self.column = int(str(self.value)[1])

            if self.value == self.concatenate_index:
                return cell_list
            elif len(cell_list) == 5 * 5:
                raise Exception('NO TREASURE')


class InputData:
    """User input data"""

    def __init__(self):
        self.data = []

    def add_data(self, line):
        self.data.append([int(i) for i in line.split()])
        return self.data


if __name__ == "__main__":

    input_data = InputData()
    for _ in range(5):
        input_data.add_data(input())

    map1 = TreasureMap(input_data.data)
    treasure_map1 = map1.get_treasure_map()

    treasure1 = Treasure()
    result = treasure1.find_treasure(treasure_map1)
    print(result)
